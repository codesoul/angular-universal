import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(private http: HttpClient) { }


  getAllUsersList(){
    let params = new HttpParams();
    params = params.append("page",'1')
    return this.http.get('https://reqres.in/api/users',{params});
  }

  createNewUser(formdata): Observable<any>{
    return this.http.post<any>('https://reqres.in/api/users', formdata);
  }

  deleteUser(id){
    let params = new HttpParams();
    params = params.append("id",id)
    return this.http.delete('https://reqres.in/api/users',{params});
  }

}
