import { Component, OnInit } from '@angular/core';
import { MainServiceService } from 'src/app/shared/main-service.service';

@Component({
  selector: 'app-testget',
  templateUrl: './testget.component.html',
  styleUrls: ['./testget.component.scss']
})
export class TestgetComponent implements OnInit {

  lists ;
  constructor(private service: MainServiceService) { }

  ngOnInit(): void {
    this.callingApi();
  }

  callingApi(){
    this.service.getAllUsersList().subscribe(
      (data=>{
        console.log(data)
        this.lists = data["data"]
      }),
      (error=>{

      })
    )
  }
}
