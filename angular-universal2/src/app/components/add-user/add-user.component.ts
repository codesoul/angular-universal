import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MainServiceService } from 'src/app/shared/main-service.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {


  userform = new FormGroup({
    id: new FormControl(''),
    first_name: new FormControl(''),
    last_name: new FormControl(''),
    email: new FormControl(''),
    avatar: new FormControl(''),

  });


  constructor(private service: MainServiceService) { }

  ngOnInit(): void {
  }

  submit(){
    this.service.createNewUser(this.userform.value).subscribe(
      (data=>{
        alert("Successfully saved!!!")
      }),
      (error =>{
        alert("Not saved!!!")
      })
    )
    console.log(this.userform.value)
  }
}
